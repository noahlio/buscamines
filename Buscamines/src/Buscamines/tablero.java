package Buscamines;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;

/**
 * Classe que inicia el tauler de joc, utilitzant la classe laminaGeneralTablero
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see laminaGeneralTablero
 */
public class tablero extends JFrame{

	/**
	 * Constructor que inicia el tauler, es defineix la posici�, visibilitat, reescalabilitat...
	 */
	public tablero() {
		Dimension windowSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize((int)windowSize.getHeight()-20,(int)windowSize.getHeight()-20);
		centrar(0,0);
		setVisible(true); //visibilidad de frame
		setResizable(false);
		laminaGeneralTablero lamina=new laminaGeneralTablero(); //crear lamina
		add(lamina);//a�adir lamina
	}    
	public void centrar(int masx, int masy) {
        Dimension windowSize = getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x - windowSize.width / 2;
        int dy = centerPoint.y - windowSize.height / 2;
        setLocation(dx+masx, dy+masy);
    }
}
//-----------------------------------------------------------------------------------
/**
 * Llamina general del tauler, conte dues llamines amb el contingut
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see laminaTiempoTablero
 * @see laminaMinas
 *
 */
class laminaGeneralTablero extends JPanel{
	public laminaGeneralTablero() {
		setLayout(new BorderLayout());
		add(new laminaTiempoTablero(),BorderLayout.NORTH);
		add(new laminaMinas(),BorderLayout.CENTER);
	}
}
/**
 * Classe que crea objectes tipus label transparent, amb una ImageIcon
 * @author Noah
 * @version 2.0
 * @since 2.0
 *
 */
class labelTransparent extends JLabel{
	/**
	 * Constructor que crea el label transparent amb una icona
	 * @param icono ImageIcon per a assignar al label
	 */
	public labelTransparent(ImageIcon icono) {
		setFont(new Font("Arial",Font.PLAIN,30));
		setText(jugadors.getMinas()+"  ");
		setIcon(icono);
		setBorder(null);
	}
}
//-----------------------------------------------------------------------------------
/**
 * Llamina on es mostren atributs de partida (temps i banderes restants)
 * @author Noah
 * @version 2.0
 * @since 1.0
 *
 */
class laminaTiempoTablero extends JPanel{
	/**
	 * Constructor que crea un cronometre per controlar el temps de partida i un label transparent per a mostrar les banderes restants
	 */
	public laminaTiempoTablero() {
		setLayout(new BorderLayout());
		//----------------------------------------------------------------
		long milisegundos=System.currentTimeMillis();
		tiempo =new JLabel("0");
		tiempo.setFont(new Font("Arial",Font.PLAIN,40));
		tiempo.setHorizontalAlignment(JLabel.CENTER);
		timer=new Timer();
		TimerTask tarea=new TimerTask() {
			public void run() {
				long time=(System.currentTimeMillis()-milisegundos)/1000;
				tiempo.setText(String.valueOf(time));
			}
		};
		timer.schedule(tarea, 0,1000);
		//----------------------------------------------------------------
		ImageIcon icone=metodesImages.icon(new ImageIcon(".//images//bandera.png"),40,40);
		banderas=new labelTransparent(icone);
		//----------------------------------------------------------------
		add(tiempo,BorderLayout.CENTER);
		add(banderas,BorderLayout.EAST);
	}
	/**
	 * Metode que canvia el text del temps segons el parametre que torni el rellotge
	 * @param a Numero de segons que porta en partida
	 */
	public static void newText(String a) {
		timer.cancel();
		tiempo.setText(a);
	}
	/**
	 * Metode que defineix el text de les banderes segons si s'ha possat o tret una bandera
	 * @param a true -- s'ha tret bandera, false el contrari
	 */
	public static void sumBandera(boolean a) {
		laminaTiempoTablero.banderas.setText(String.valueOf(Integer.parseInt(laminaTiempoTablero.banderas.getText().strip())+(a?1:-1))+"  ");
	}
	/**
	 * label amb una imatge mostrant les banderes restants
	 */
	static labelTransparent banderas;
	/**
	 * Rellotge de partida
	 */
	static Timer timer;
	/**
	 * Label amb el temps de partida
	 */
	static JLabel tiempo;
}
//-----------------------------------------------------------------------------------
/**
 * Classe amb metodes utilitzats en imatges
 * @author Noah
 * @version 2.0
 * @since 2.0
 *
 */
class metodesImages{
	public static ImageIcon icon(ImageIcon i, int x, int y) {
		return new ImageIcon(i.getImage().getScaledInstance(x, y, java.awt.Image.SCALE_SMOOTH));
	}
}
/**
 * Classe amb el tauler de mines i els metodes per a editar el contingut
 * @author Noah
 * @version 2.0
 * @since 1.0
 *
 */
class laminaMinas extends JPanel{
	static boolean win;
	public laminaMinas() {
		win=false;
		partida.inicio=false;
		int dimensiones=jugadors.getDimensiones();
		minas=new mina[dimensiones][dimensiones];
	    setLayout(new GridLayout(dimensiones, dimensiones));
	    for(int x=0;x<dimensiones;x++) {
	    	for(int y=0;y<dimensiones;y++) {
	    		mina m=new mina(x,y);
	    		m.setMargin(new Insets(0, 0, 0, 0));
	    		m.setBackground(new Color(50,50,50));//gris oscuro
	    		m.addActionListener(new clickBomba(x,y));
	    		m.addMouseListener(new clickBandera(x,y));
	    		minas[x][y]=m;
	    		add(m);
	    	}
	    }
	}
	/**
	 * Metode per a pintar els botons (en cas de perdre)
	 */
	public static void pintarButtons() {
		for(mina[] m:minas) {
			for(mina mm:m) {
				if(mm.explota) mm.setBackground(Color.red);
			}
		}
	}
	/**
	 * Metode per a saber les dimensions del tauler
	 * @return dimensions del tauler de mines
	 */
	public static int getDim() {
		return minas.length;
	}
	/**
	 * Classe interna que posara i treura banderes. 
	 * @author Noah
	 * @version 2.0
	 * @since 1.0
	 *
	 */
	private class clickBandera implements MouseListener{
		int x;
		int y;
		/**
		 * Constructor que defineix les coordenades de la mina seleccionada
		 * @param x Coordenada x
		 * @param y Coordenada y
		 */
		public clickBandera(int x, int y) {
			this.x=x;
			this.y=y;
		}
		public void mouseClicked(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {}
		/**
		 * Metode que posa o treu bandera al donar click dret a una mina
		 */
		public void mouseReleased(MouseEvent e) {
			if(SwingUtilities.isRightMouseButton(e)) {
				if(!win) {
					if(!minas[x][y].marcada && minas[x][y].getIcon()==null) {
						minas[x][y].setIcon(metodesImages.icon(new ImageIcon(".//images//bandera.png"),minas[x][y].getSize().width*2/3, minas[x][y].getSize().height*2/3));
						minas[x][y].setEnabled(false);
						laminaTiempoTablero.sumBandera(false);
					}
					else if(!(minas[x][y].getIcon()==null)) {
						minas[x][y].setIcon(null);
						minas[x][y].setEnabled(true);
						laminaTiempoTablero.sumBandera(true);
					}
				}
			}
		}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		
	}
	/**
	 * Classe privada que fa acciona els sons de les caselles. Un soroll diferent segons si era mina o no
	 * @author Noah
	 * @version 2.0
	 * @since 2.0
	 *
	 */
	private class clickBomba implements ActionListener{
		int x;
		int y;
		public clickBomba(int x, int y) {
			this.x=x;
			this.y=y;
		}
		public void actionPerformed(ActionEvent e) {
			if(minas[x][y].explota) sonidito.play(".//sound//aaa.wav");
			else sonidito.play(".//sound//pop.wav");
			partida.start(x, y);
		}
	}
	/**
	 * Tauler de mines
	 */
	static mina[][] minas;
}