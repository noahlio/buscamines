package Buscamines; 

import javax.swing.*;

/**
 * Classe frame que cont� una llamina ranking central
 * @author Noah
 * @see rankingCentral 
 * @version 2.0
 * @since 2.0
 */
public class ranking extends JFrame{
	public ranking() {
		setSize(400,300); //frame con la mitad de tama�o
		rankingCentral rc=new rankingCentral();
		add(rc);
		setVisible(true);
	}
}

/**
 * Classe panel que cont� un llistat del jugadors amb el nom i les seves victories
 * @author Noah
 * @version 2.0
 * @since 2.0
 */
class rankingCentral extends JPanel{
	/**
	 * constructor que anira afegint els jugadors al ranking.
	 */
	public rankingCentral() {
		general =Box.createVerticalBox();
		for(int x=0;x<jugadors.jugadores.size();x++) {
			addUser(jugadors.jugadores.get(x));
		}
		add(general);
	}
	static Box general;
	/**
	 * Metode per a afegir jugadors al ranking
	 * @param j jugador que es ficara al ranking
	 */
	public void addUser(jugador j) {
		Box individual=Box.createHorizontalBox();
		JLabel nombre=new JLabel("Nombre: "+j.nom);
		JLabel victorias=new JLabel("Victorias: "+j.victories);
		individual.add(nombre);
		individual.add(Box.createHorizontalStrut(10));
		individual.add(victorias);
		general.add(individual);
	}
}