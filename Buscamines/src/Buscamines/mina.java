package Buscamines;
import java.awt.*;
import javax.swing.*;


/**
 * Classe on es defineixen objectes de tipus mina, aquests objectes poden
 * ser explosius o segurs. Es desen en una matriu de mines a la clase laminaMinas
 * @author Noah
 * @version 2.0
 * @see laminaMinas
 * @since 1.0
 *
 */
public class mina extends JButton{ 
	/**
	 * Llista dels colors asignables al text (num de mines al voltant)
	 * @see setMinasAprox
	 */
	Color[] colores= {Color.blue,
			Color.green,
			Color.red,
			new Color(0,23,134),//dark blue
			new Color(96,53,18),//brown
			new Color(18,96,27),//dark green
			Color.ORANGE,
			Color.black
			};
	/**
	 * booleana per a saber si la mina explota
	 */
	boolean explota=false;
	/**
	 * booleana per a saber si aquesta mina ha sigut marcada
	 */
	boolean marcada=false;
	/**
	 * Booleana per a saber si aquesta mina ya ha fet recursivitat
	 */ 
	boolean recur=false;
	/**
	 * Quantitat de minas al voltant
	 */
	int minasAprox;
	/**
	 * Posici� x de la mina
	 */
	int x;
	/**
	 * Posici� y de la mina
	 */
	int y;
	/**
	 * 
	 * Aquest constructor defineix les coordenades de la mina
	 * @param x posici� x de la mina
	 * @param y posici� y de la mina
	 */
	public mina(int x, int y) {
		this.x=x;
		this.y=y;
	}
	/**
	 * Metode per saber si la mina no esta marcada
	 * @return torna una booleana dient si no ha estat marcada (true -- no marcada)
	 */
	public boolean noMarcada() {
		return marcada;
	}
	/**
	 * Metode per saber si la casella es pot seleccionar.
	 * @return torna una booleana dient si la casella no explota y no esta marcada, es a dir, que es una casella valida per a marcar
	 */
	public boolean minaLimpia() {
		return !marcada&&!explota;
	}
	/**
	 * Metode que defineix la casella com a una casella marcada
	 */
	public void marcar() {
		marcada=true;
	}
	/**
	 * Metode per a definir les mines que hi ha al voltant, es cambiara el color y text
	 * @param minasAprox la cuantitat de minas que hi ha al voltant de la casella
	 */
	public void setMinasAprox(int minasAprox){
		this.minasAprox=minasAprox;
		laminaMinas.minas[x][y].setBackground(Color.white); //marron flojo
		if(minasAprox!=0) {
			laminaMinas.minas[x][y].setForeground(colores[minasAprox-1]);
			laminaMinas.minas[x][y].setText(String.valueOf(minasAprox));
			laminaMinas.minas[x][y].setFont(new Font("Arial",Font.PLAIN,25));
		}
	}

}
