package Buscamines;

/**
 * La classe jugador serveix per crear objectes d'aquest tipus i 
 * desar endins la llista jugadores de la clase jugadors
 * @author Noah
 * @see jugadors
 * 
 */
public class jugador {
	String nom;
	int victories=0;
	int millorTemps=Integer.MAX_VALUE;
	int numDimensiones=8;
	int numMinas=10;
	int dificultad;
	/**
	 * constructor de objectes tipus jugador, es definira el nom y la dificultad per defecte
	 * @param nom Nom del jugador creat
	 */
	public jugador(String nom) {
		this.nom=nom;
		dificultad=0;
	}
}