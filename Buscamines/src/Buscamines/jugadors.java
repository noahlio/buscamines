package Buscamines;

import java.util.*;

/**
 * Classe on s'enmagatzemen objectes tipus jugador. A mes hi ha metodes per a recollir dades dels jugadors.
 * @author Noah
 * @version 2.0
 * @see jugador
 * @since 1.0
 *
 */
public class jugadors { 
	/**
	 * Llista amb el jugadors
	 */
	static ArrayList<jugador> jugadores;
	/**
	 * jugador que esta jugant en aquest moment
	 */
	static jugador jugadorActual;
	/**
	 * posici� del jugador dins de la llista
	 */
	static int posJugador;
	/**
	 * metode que afegeix un jugador a la llista de jugadors
	 * @param j jugador que s'afegira a jugadors
	 */
	public static void addJugador(jugador j) {
		jugadores.add(j);
	}
	/**
	 * funci� que inicia la llista de jugadors, afegeix el primer jugador i defineix el jugador actual.
	 * @param j jugador que es definira com a actual i s'afegira a la llista
	 */
	public static void iniciar(jugador j) {
		jugadores=new ArrayList<jugador>();
		jugadores.add(j);
		jugadorActual=j;
		posJugador=0;
	}
	/**
	 * Aquest metode cambia el jugador per un altre dins de la llista de jugadors
	 * @param x posici� del jugador actual
	 */
	public static void setJugadorActual(int x) {
		jugadorActual=jugadores.get(x);
	}
	/**
	 * Metode per a definir les dimensions de tauler
	 * @param x numero de dimensions que tindra el tauler del buscamines en aquest jugador
	 */
	public static void setDimensiones(int x) {
		jugadorActual.numDimensiones=x;
	}
	/**
	 * Metode per a definir les mines amb les que jugara el jugador
	 * @param x numero de mines que es definira en el jugador
	 */
	public static void setMinas(int x) {
		jugadorActual.numMinas=x;
	}
	/**
	 * Metode per consultar les dimensions de tauler d'un jugador
	 * @return Numero de dimensions
	 */
	public static int getDimensiones() {
		return jugadorActual.numDimensiones;
	}
	/**
	 * Metode per consultar les mines de tauler d'un jugador
	 * @return Numero de mines
	 */
	public static int getMinas() {
		return jugadorActual.numMinas;
	}
	/**
	 * Metode per comprovar si un jugador ja existeix en la llista de jugadors
	 * @return True o false, segons si el jugador existeix
	 */
	public static boolean nombreExiste(String nombre) {
		boolean valid=false;
		int x=0;
		while(valid==false && x<jugadores.size()) {
			if(jugadores.get(x).nom.equals(nombre)) valid=true;
			x++;
		}
		return valid;
	}
	/**
	 * Metode que canvia de jugador al que esta al seu costat
	 * @param a booleana que diu si es llisca a la dreta o a l'esquerra (true : dreta -- false _ esquerra)
	 */
	public static void movePlayer(boolean a) {
		posJugador+=a?1:-1;
		jugadorActual=jugadores.get(posJugador);
	}
}
