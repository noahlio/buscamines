package Buscamines;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
//----------------------------------------------------------------------------------------------
/**
 * Classe que construeix el marc principal del menu d'opcions. Cont� la llamina laminaLayoutOpciones
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see laminaLayoutOpciones
 */
public class opciones extends JFrame{
	/**
	 * Constructor que defineix la mida, posici�, reescalabilitat, visibilitat i llamina
	 */
	public opciones() {
		setSize(400,300); //frame con la mitad de tamaño
		centrar(0,50);
		setResizable(false);
		laminaLayoutOpciones lamina=new laminaLayoutOpciones(); //crear lamina
		add(lamina);//añadir lamina
		setVisible(true); //visibilidad de frame
	}
	/**
	 * Metode que centra el marc
	 * @param masx Coordenada x allunyada del centre
	 * @param masy Coordenada y allunyada del centre
	 */
	public void centrar(int masx, int masy) {
        Dimension windowSize = getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x - windowSize.width / 2;
        int dy = centerPoint.y - windowSize.height / 2;
        setLocation(dx+masx, dy+masy);
    }
}
//----------------------------------------------------------------------------------------------
/**
 * Llamina principal del menu d'opcions. Cont� les llamines laminaNorteOpciones, laminaNiveles, laminaNuevoJugador, laminaSiguienteJugador i laminaJugadorAnterior
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see laminaNorteOpciones
 * @see laminaNiveles
 * @see laminaNuevoJugador
 * @see laminaSiguienteJugador
 * @see laminaJugadorAnterior
 */
class laminaLayoutOpciones extends JPanel{
	/**
	 * Constructor que posiciona les llamines internes
	 */
	public laminaLayoutOpciones() {
		setLayout(new BorderLayout());
		add(new laminaNorteOpciones(),BorderLayout.NORTH);
		add(new laminaNiveles(),BorderLayout.CENTER);
		add(new laminaNuevoJugador(),BorderLayout.SOUTH);
		add(new laminaSiguienteJugador(),BorderLayout.EAST);
		add(new laminaJugadorAnterior(),BorderLayout.WEST);
	}
}
//----------------------------------------------------------------------------------------------
/**
 * Llamina on es mostra el nom del jugador actual
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see jugadors
 *
 */
class laminaNorteOpciones extends JPanel{
	/**
	 * Constructor que defineix el tipus de lletra i el nom
	 */
	public laminaNorteOpciones() {
		nombre=new JLabel(jugadors.jugadorActual.nom);
		nombre.setFont(new Font("Arial",Font.PLAIN,20));
        nombre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		add(nombre);
	}
	/**
	 * Metode que refresca el nom del jugador actual
	 */
	public static void refresh() {
		nombre.setText(jugadors.jugadorActual.nom);
	}
	/**
	 * Label amb al nom del jugador actual
	 */
	static JLabel nombre;
}
//----------------------------------------------------------------------------------------------
/**
 * Classe que s'encarrega de tenir llamines filles i un jspinner que defineix el nivell de dificultat
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see laminaNumDimensiones
 * @see laminaNumMinas
 *
 */
class laminaNiveles extends JPanel{
	/**
	 * Constructor que posiciona les llamines filles i un jspinner amb el nivell de dificultat
	 */
	public laminaNiveles() {
		setLayout(new BorderLayout());
		//--------------------------------------------
		nivel=new JSpinner(new SpinnerListModel(niveles));
		nivel.addChangeListener(new oyenteNivel());
		updateState(jugadors.jugadorActual.dificultad);
		//bloquear cambio texto
	    JFormattedTextField tf = ((JSpinner.DefaultEditor) nivel.getEditor()).getTextField();
	    tf.setEditable(false);
	    //alinear texto centro
		JComponent editor = nivel.getEditor();
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor)editor;
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		add(nivel,BorderLayout.CENTER);
		//--------------------------------------------
		add(new laminaNumDimensiones(),BorderLayout.SOUTH);
		//--------------------------------------------
		add(new laminaNumMinas(),BorderLayout.NORTH);
	}
	/**
	 * Classe interna que truca al metode updateState amb el nivell de dificultat que ha seleccionat l'usuari
	 * @author Noah
	 * @version 2.0
	 * @since 1.0
	 * @see updateState
	 */
	private class oyenteNivel implements ChangeListener{
		public void stateChanged(ChangeEvent e) {
			int x=0;
			while(niveles[x]!=nivel.getValue().toString())x++;
			updateState(x);
		}
	}
	/**
	 * Metode per a canviar el nivell de dificultat i habilitar (nivell personalitzat)/deshabilitar (nivell no personalitzat) els spinners.
	 * @param niv nivell de dificultat
	 */
	public static void updateState(int niv) {
		nivel.setValue(niveles[niv]);
		if(nivel.getValue().toString().equals("PERSONALITZAT")) {
			laminaNumMinas.enabled(true);
			laminaNumDimensiones.enabled(true);
		}
		else {
			laminaNumMinas.enabled(false);
			laminaNumMinas.setMinas(minas[niv]);
			laminaNumDimensiones.enabled(false);
			laminaNumDimensiones.setDimensiones(dimensiones[niv]);
			jugadors.setDimensiones(dimensiones[niv]);
			jugadors.setMinas(minas[niv]);
		}
		jugadors.jugadorActual.dificultad=niv;
	}
	/**
	 * spinner amb el nivell del jugador
	 */
	static JSpinner nivel;
	/**
	 * Llista de nivells disponibles
	 */
	static String[] niveles={"PRINCIPIANT", "INTERMIG", "EXPERT", "PERSONALITZAT"};
	/**
	 * Llista de mines disponibles
	 */
	static int[] minas= {10,40,99};
	/**
	 * Llista de dimensions disponibles
	 */
	static int[] dimensiones= {8,16,30};
}
//----------------------------------------------------------------------------------------------
/**
 * Classe amb el numero de mines amb les que es jugara
 * @author Noah
 * @version 2.0
 * @since 1.0
 *
 */
class laminaNumMinas extends JPanel{
	/**
	 * Constructor que defineix els elements de la llamina
	 */
	public laminaNumMinas() {
		JLabel nºminas = new JLabel("  Mines: ");
		add(nºminas);
		numMinas.setEditor(new JSpinner.DefaultEditor(numMinas));
		numMinas.addChangeListener(new cambio());
		//dimensiones
		Dimension d=new Dimension(220,25);
		numMinas.setPreferredSize(d);
		//añadir
		add(numMinas);
	}
	/**
	 * Classe que cambia les mines del jugador
	 * @author Noah
	 * @version 2.0
	 * @since 1.0
	 *
	 */
	private class cambio implements ChangeListener{
		public void stateChanged(ChangeEvent e) {
			jugadors.setMinas((int) numMinas.getValue());
		}
	}
	/**
	 * Metode que habilita o deshabilita el jspinner numMinas
	 * @see numMinas
	 * @param a Booleana que defineix si s'habilita o no
	 */
	public static void enabled(boolean a) {
		numMinas.setEnabled(a);
	}
	/**
	 * Metode per a que al canviar el numero de dimensions el numero de mines no s'ensurti del maxim o minim.
	 */
	public static void change() {
		int max=getMaxValue();
		int min=getMinValue();
		int value=(int)numMinas.getValue();
		if(value>max) numMinas.setValue(max);
		else if(value<min) numMinas.setValue(min);
	}
	/**
	 * Metode per a definir el numero de mines al jspinner numMinas
	 * @param i Numero de mines
	 */
	public static void setMinas(int i) {
		numMinas.setValue(i);
	}
	/**
	 * Metode per a saber el numero minim de mines
	 * @return Numero minim de mines
	 */
	public static int getMinValue() {
		return (int) (Math.pow(laminaNumDimensiones.getDimensions(),2)/10);
	}
	/**
	 * Metode per a saber el numero maxim de mines
	 * @return Numero maxim de mines
	 */
	public static int getMaxValue() {
		return (int) (Math.pow(laminaNumDimensiones.getDimensions(),2)/5);
	}
	/**
	 * Metode per a saber el numero de mines
	 * @return Numero de mines
	 */
	public static int getValue() {
		return (int) numMinas.getValue();
	}
	/**
	 * Jspinner amb el numero de mines al tauler
	 */
	static JSpinner numMinas=new JSpinner(new JSpinnerMaxVar(1,1,1000,1));;
}	
//CLASE HEREDADA PARA HACER UN LIMITE SOBREESCRIBIENDO GETNEXTVALUE
class JSpinnerMaxVar extends SpinnerNumberModel {
	public JSpinnerMaxVar(int a, int b, int c, int d) {
		super(a, b, c, d);// iniciar el padre (SpinnerNumberModel) con los parametros a,b,c,d
	}
	public Object getNextValue() {// Sobreescribir metodo getNextValue
		if(laminaNumMinas.getValue()<laminaNumMinas.getMaxValue()) return super.getNextValue(); // devolver el metodo previousValue del padre
		else return laminaNumMinas.getValue();
	}
}
//----------------------------------------------------------------------------------------------
class laminaNumDimensiones extends JPanel{
	public laminaNumDimensiones() {
		JLabel ndim = new JLabel("  Dimensions: ");
		add(ndim);
		numDimensiones.addChangeListener(new cambio());
		numDimensiones.setEditor(new JSpinner.DefaultEditor(numDimensiones));
		//dimensiones
		Dimension d=new Dimension(200,25);
		numDimensiones.setPreferredSize(d);
		//añadir
		add(numDimensiones);
	}
	private class cambio implements ChangeListener{
		public void stateChanged(ChangeEvent e) {
			jugadors.setDimensiones((int) numDimensiones.getValue());
			laminaNumMinas.change();
		}
	}
	public static void enabled(boolean a) {
		numDimensiones.setEnabled(a);
	}
	public static int getDimensions() {
		return (Integer)numDimensiones.getValue();
	}
	public static void setDimensiones(int i) {
		numDimensiones.setValue(i);
	}
	static JSpinner numDimensiones=new JSpinner(new SpinnerNumberModel(4,4,50,1));
}
//----------------------------------------------------------------------------------------------
class laminaNuevoJugador extends JPanel{
	public laminaNuevoJugador() {
		setLayout(new FlowLayout());
		nombre=new JTextField(20);
		JButton nuevoJugador=new JButton("Crear nou jugador");
		nuevoJugador.addActionListener(new crearJugador());
		add(nombre);
		add(nuevoJugador);
	}
	private class crearJugador implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String nom=nombre.getText();
			if(nom.equals("")||jugadors.nombreExiste(nom)) {
				nombre.setForeground(Color.red);
			}
			else {
				nombre.setForeground(Color.black);
				jugador j=new jugador(nom);
				jugadors.addJugador(j);
				nombre.setText("");
				laminaSiguienteJugador.jugadorDerecha.setEnabled(true);
			}
		}
	}
	JTextField nombre;
}
//----------------------------------------------------------------------------------------------
class laminaSiguienteJugador extends JPanel{
	public laminaSiguienteJugador() {
		setLayout(new BorderLayout());
		jugadorDerecha=new JButton("->");
		enabled();
		jugadorDerecha.addActionListener(new moverJugador());
		add(jugadorDerecha);
	}
	private class moverJugador implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(jugadors.posJugador<jugadors.jugadores.size()-1) {
				jugadors.movePlayer(true);
				laminaBienvenidaJugador.refresh();
				laminaNorteOpciones.refresh();
				enabled();
				laminaJugadorAnterior.enabled();
				laminaNumMinas.setMinas(jugadors.getMinas());
				laminaNumDimensiones.setDimensiones(jugadors.getDimensiones());
				laminaNiveles.updateState(jugadors.jugadorActual.dificultad);
			}
		}
	}
	public static void enabled() {
		jugadorDerecha.setEnabled(jugadors.posJugador!=jugadors.jugadores.size()-1);
	}
	static JButton jugadorDerecha;
}
//----------------------------------------------------------------------------------------------
class laminaJugadorAnterior extends JPanel{
	public laminaJugadorAnterior() {
		setLayout(new BorderLayout());
		jugadorIzquierda=new JButton("<-");
		enabled();
		jugadorIzquierda.addActionListener(new moverJugador());
		add(jugadorIzquierda);
	}
	private class moverJugador implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(jugadors.posJugador>0) {
				jugadors.movePlayer(false);
				laminaBienvenidaJugador.refresh();
				laminaNorteOpciones.refresh();
				laminaSiguienteJugador.enabled();
				enabled();
				laminaNumMinas.setMinas(jugadors.getMinas());
				laminaNumDimensiones.setDimensiones(jugadors.getDimensiones());
				laminaNiveles.updateState(jugadors.jugadorActual.dificultad);
			}
		}
	}
	public static void enabled() {
		jugadorIzquierda.setEnabled(jugadors.posJugador!=0);
	}
	static JButton jugadorIzquierda;
}