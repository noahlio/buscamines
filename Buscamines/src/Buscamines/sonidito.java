package Buscamines;

import java.io.*;
import javax.sound.sampled.*;
/**
 * Classe amb la que s'afegeixen efectes de so, s'utilitza al tauler quan es pulsa en una casella
 * @author Noah
 * @version 2.0
 * @see tablero
 * @since 2.0
 */
public class sonidito {
	/**
	 * objecte on esta definida la ruta del arxiu de so y  
	 */
	static Clip clip;
	/**
	 * metode per a definir la ruta on esta l'arxiu de so, es defineix al objecte clip
	 * @param fileName ruta del arxiu de so.
	 */
	public static void setFile(String fileName) {
		try {
			File file=new File(fileName);
			AudioInputStream sonido=AudioSystem.getAudioInputStream(file);
			clip=AudioSystem.getClip();
			clip.open(sonido);
		}
		catch(Exception e) {
			System.out.println("a");
		}
	}
	/**
	 * Metode que reprodueix el clip
	 * @param fileName ruta del arxiu de so.
	 */
	public static void play(String fileName) {
		setFile(fileName);
		clip.setFramePosition(0);
		clip.start();
	}
}
