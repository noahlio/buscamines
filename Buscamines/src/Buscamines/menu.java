package Buscamines;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Classe inicial que iniciara el marc del menu
 * @author Noah
 * @version 2.0
 * @since 1.0
 * @see marco
 *
 */
public class menu { //inicia el marc amb el menu
	public static void main(String[] args) {
		/**
		 * Jugador per defecte
		 */
		jugadors.iniciar(new jugador("guest"));
		marco m1=new marco();
	}
}
//----------------------------------------------------------------------------------------------
/**
 * Marc principal del buscamines, mostra el menu.
 * @author Noah
 * @version 2.0
 * @see laminaLayoutMenu
 * @since 1.0
 */
class marco extends JFrame{
	public marco() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //accion al cerrar frame
		setTitle("main");
		setSize(400,100); //frame con la mitad de tama�o
		centrar(0,0);
		setResizable(false);
		laminaLayoutMenu lamina=new laminaLayoutMenu(); //crear lamina
		add(lamina);//a�adir lamina
		setVisible(true); //visibilidad de frame
	}
    public void centrar(int masx, int masy) {
        Dimension windowSize = getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x - windowSize.width / 2;
        int dy = centerPoint.y - windowSize.height / 2;
        setLocation(dx+masx, dy+masy);
    }
}
//----------------------------------------------------------------------------------------------
/**
 * Llamina que mostra la lamina de benvinguda i la central del menu
 * @author Noah
 * @version 2.0
 * @see laminaBienvenidaJugador
 * @see laminaCentralMenu
 * @since 1.0
 *
 */
class laminaLayoutMenu extends JPanel{//clase de lamina
	public laminaLayoutMenu() {//constructor
		//cambiar disposicion de la lamina por bloques, separacion x=5, separacion y=100
		setLayout(new BorderLayout());
		/*poner disposicion de esta lamina centrada*/
		add(new laminaBienvenidaJugador(),BorderLayout.NORTH);
		add(new laminaCentralMenu(),BorderLayout.CENTER);
	}
}
//----------------------------------------------------------------------------------------------
/**
 * Llamina de benvinguda al jugador, mostra un missatge de benvinguda amb el seu nom
 * @author Noah
 * @version 2.0
 * @since 1.0
 *
 */
class laminaBienvenidaJugador extends JPanel{
	public laminaBienvenidaJugador() {
		setLayout(new BorderLayout());
		nombre=new JLabel("Benvingut "+jugadors.jugadorActual.nom);
		nombre.setFont(new Font("Arial",Font.PLAIN,20));
        nombre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		add(nombre);
	}
	public static void refresh() {
		nombre.setText("Benvingut "+jugadors.jugadorActual.nom);
	}
	/**
	 * label amb un text donant la benvinguda al jugador
	 * @see jugador
	 */
	static JLabel nombre;
}
//----------------------------------------------------------------------------------------------
/**
 * Llamina amb els botons de menu, aquests obren tots els apartats del buscamines.
 * @author Noah
 * @version 2.0
 * @since 1.0
 *
 */
class laminaCentralMenu extends JPanel{
	public laminaCentralMenu() {//constructor
		/*poner disposicion de esta lamina centrada*/
		JButton help=new JButton("Ajuda");
		JButton opcions=new JButton("Opcions");
		JButton jugar=new JButton("Jugar");
		JButton ranking=new JButton("Ranking");
		help.addActionListener(new abrirAyuda());
		opcions.addActionListener(new abrirOpciones());
		jugar.addActionListener(new abrirJuego());
		ranking.addActionListener(new abrirRanking());
		add(help);
		add(opcions);
		add(jugar);
		add(ranking);
	}
	/**
	 * Oient que obre un message dialog
	 * @author Noah
	 * @version 2.0
	 * @since 1.0
	 *
	 */
	private class abrirAyuda implements ActionListener{
		/**
		 * Metode principal heretat de la interficie ActionListener, te la funci� 
		 * d'obrir un missatge d'ajuda al detectar una acci� al bot� assignat
		 */
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(
				    null, 
				    "el buscamines es un joc de buscar mines", 
				    "Ajuda",
				    JOptionPane.INFORMATION_MESSAGE); 
		}
	}
	/**
	 * Oient que obre el menu d'opcions
	 * @author Noah
	 * @version 2.0
	 * @see opciones
	 * @since 1.0
	 *
	 */
	private class abrirOpciones implements ActionListener{
		/**
		 * Metode principal heretat de la interficie ActionListener, te la funci� 
		 * d'obrir el menu d'opcions al detectar una acci� al bot� assignat
		 */
		public void actionPerformed(ActionEvent e) {
			opciones menuOpciones=new opciones();
		}
	}
	/**
	 * Oient que obre el tauler de joc en cas de no haver-hi un altre obert, 
	 * si hi ha un altre mostra un message dialog informant que s'ha de tancar el joc anterior
	 * @author Noah
	 * @version 2.0
	 * @see tablero
	 * @since 1.0
	 *
	 */
	private class abrirJuego implements ActionListener{
		/**
		 * Metode principal heretat de la interficie ActionListener, te la funci� 
		 * d'obrir el joc al detectar una acci� al bot� assignat. En cas d'haver-hi un
		 * joc en marxa sortira un missatge informant del error.
		 */
		public void actionPerformed(ActionEvent e) {
			if(t==null) {
				t=new tablero();
				t.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						partida.minasLimpias=0;
						t=null;
						laminaTiempoTablero.timer.cancel();
					}
				});
			}
			else JOptionPane.showMessageDialog(
				    null, 
				    "Tanca la partida actual per crear una nova", 
				    "Ajuda",
				    JOptionPane.INFORMATION_MESSAGE); 
		}
	}
	/**
	 * Oient que obre el ranking
	 * @author Noah
	 * @version 2.0
	 * @see ranking
	 * @since 1.0
	 *
	 */
	private class abrirRanking implements ActionListener{
		/**
		 * Metode principal heretat de la interficie ActionListener, te la funci� 
		 * d'obrir el ranking al detectar una acci� al bot� assignat
		 */
		public void actionPerformed(ActionEvent e) {
			menuRanking=new ranking();
		}
	}
	/**
	 * objecte de tipus tablero, on hi es tot l'apartat jugable
	 * @see tablero
	 */
	static tablero t;
	/**
	 * objecte de tipus manuRanking on hi es tot el ranking de jugadors
	 * @see menuRanking
	 */
	static ranking menuRanking;
}
