package Buscamines;

import java.util.*;

/**
 * classe que cont� tot el codi que es mostra a la classe tablero.
 * @author Noah
 * @version 2.0
 * @see tablero
 * @since 1.0
 */
public class partida {
	/**
	 * Funci� que inicia les mines en cas de ser la primera jugada, si no fa recursivitat en la matriu de mines
	 * @param x posici� x d'on es fa la recursivitat en la matriu de mines
	 * @param y posici� y d'on es fa la recursivitat en la matriu de mines
	 */
	public static void start(int x,int y) {
		if(haEmpezado()) recursividad(x,y);
		else iniciarMinas(x,y);
	}
	/**
	 * Metode que fa el primer cuadrat recursiu, aquestes mines no explotaran mai. Despres truca a activarMines i fa la recursivitat
	 * @param x Coordenada x seleccionada
	 * @param y Coordenada y seleccionada
	 */
	public static void iniciarMinas(int x, int y) {
		for(int i=-1;i<2;i++) {
			for(int j=-1;j<2;j++) {
				if(in(x+i,y+j)) laminaMinas.minas[x+i][y+j].marcar();
			}
		}
		activarMinas();
		recursividad(x,y);
	}
	/**
	 * Metode que activa les mines del tauler, evitant les caselles on s'ha definit el cuadrat segur (definit al metode iniciarMinas)
	 */
	public static void activarMinas() {
		Random r=new Random();
		int numMinas=jugadors.getMinas();
		while(numMinas>0) {
			int x=r.nextInt(laminaMinas.getDim());
			int y=r.nextInt(laminaMinas.getDim());
			if(laminaMinas.minas[x][y].minaLimpia()) {
				laminaMinas.minas[x][y].explota=true;
				numMinas--;
			}
		}
	}
	/**
	 *Metode recursiu que destapa les caselles del costat de la seleccionada que no siguin mines.
	 * @param x Coordenada x seleccionada
	 * @param y Coordenada y seleccionada
	 */
	public static void recursividad(int x, int y) {
		if(laminaMinas.minas[x][y].getIcon()!=null) {
			laminaMinas.minas[x][y].setIcon(null);
			laminaTiempoTablero.sumBandera(true);
		}
		if(laminaMinas.minas[x][y].explota) lost();
		else if(laminaMinas.minas[x][y].recur==false){
			int minasAprox=accionesClick(x,y);
			if(Math.pow(jugadors.getDimensiones(),2)-minasLimpias==jugadors.getMinas()) win();
			else {
				if(minasAprox==0) {
					for(int i=-1;i<2;i++) {
						for(int j=-1;j<2;j++) {
							if(in(x+i,y+j)) {
								recursividad(x+i,y+j);
							}
						}
					}
				}
			}
		}
	}
	/**
	 * Metode amb totes les funcions que es fan al fer click en una casella
	 * @param x Coordenada x seleccionada
	 * @param y Coordenada y seleccionada
	 * @return torna la quantitat de mines que te al voltant
	 */
	public static int accionesClick(int x,int y) {
		laminaMinas.minas[x][y].recur=true;
		minasLimpias++;
		int minasAprox=cantidadMinas(x,y);
		laminaMinas.minas[x][y].setMinasAprox(minasAprox);
		laminaMinas.minas[x][y].marcada=true;
		laminaMinas.minas[x][y].setEnabled(true);
		return minasAprox;
	}
	/**
	 * Metode que t'informa que has guanyat
	 */
	public static void win() {
		blockButtons();
		laminaMinas.win=true;
		laminaTiempoTablero.newText("HAS GUANYAT");
		System.out.println("HAS GUANYAT");
		jugadors.jugadorActual.victories++;
	}
	/**
	 * Metode per bloquejar tots els botons, s'utilitza quan la partida s'acaba, tant al guanyar com al perdre.
	 */
	public static void blockButtons() {
		for(int x=0;x<laminaMinas.getDim();x++) {
			for(int y=0;y<laminaMinas.getDim();y++) {
				laminaMinas.minas[x][y].setEnabled(false);
			}
		}
	}
	/**
	 * Metode que torna la quantitat de mines que hi ha al voltant d'una casella
	 * @param x coordenada x a consultar
	 * @param y coordenada y a consultar
	 * @return torna el numero de mines que hi ha al voltant
	 */
	public static int cantidadMinas(int x,int y) {
		int numMinas=0;
		for(int i=-1;i<2;i++) {
			for(int j=-1;j<2;j++) {
				if(in(x+i,y+j)) {
					if(laminaMinas.minas[x+i][y+j].explota)numMinas++;
				}
			}
		}
		return numMinas;
	}
	/**
	 * Metode que informa si s'ha perdut la partida
	 */
	public static void lost() {
		blockButtons();
		laminaMinas.pintarButtons();
		laminaTiempoTablero.newText("HAS PERDUT");
		System.out.println("HAS PERDUT");
	}
	/**
	 * Metode que et diu si una coordenada esta dins de la matriu de mines
	 * @param x posici� x a consultar
	 * @param y posici� y a consultar
	 * @return torna true si la coordenada es a dins de la matriu, false en cas contrari
	 */
	public static boolean in(int x, int y) {
		return x>=0 && x<laminaMinas.getDim() && y>=0 && y<laminaMinas.getDim();
	}
	/**
	 * Metode que et diu si es la primera jugada de la partida
	 * @return torna true si ja havia comen�at, false en cas contrari
	 */
	public static boolean haEmpezado() {
		if(inicio)return inicio;
		inicio=true;
		return false;
	}
	static boolean inicio=false;
	static int minasLimpias=0;
}
